import React, { useState } from "react";
import OnboardingForm from "./OnboardingForm";
import OtpVerification from "./OtpVerification";
import styles from '../assets/Styles/onboarding.module.css';
import Succcess from "./Succcess";

const Onboarding = () => {

  const [step, setStep] = useState('1');

  const stepOne = () => {
    setStep('2');
  }
  const stepTwo = () => {
    setStep('3');
  }

  return (
    <div>
      {step !== '3' &&
      <div className={styles.onboardingContainer}>
        <div className={styles.logo}>
          LOGO
        </div>
        <h2>
          STEP {step}
        </h2>
        <div>
          {step === '1' ?
            'Please enter your details' : 'Please enter OTP'}
        </div>
      </div> }
      {step === '1' && <OnboardingForm submittedForm={stepOne} />}
      {step === '2' && <OtpVerification submittedOtp={stepTwo} />}
      {step === '3' &&
        <Succcess />}
    </div>
  );
}

export default Onboarding;
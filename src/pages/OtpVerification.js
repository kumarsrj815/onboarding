import React, { useState } from 'react';
import styles from '../assets/Styles/otp.module.css';

const OtpVerification = (props) => {
  const [otpValues, setOtpValues] = useState(
    { value: '', otp1: '', otp2: "", otp3: "", otp4: "", otp5: "", disable: true }
  )

  const handleChange = (value1, event) => {
    setOtpValues({ [value1]: event.target.value });
  }

  const handleSubmit = (event) => {
    const data = new FormData(event.target);
    console.log(data);
    event.preventDefault();
    props.submittedOtp();
  }

  const inputfocus = (elmnt) => {
    if (elmnt.key === "Delete" || elmnt.key === "Backspace") {
      const next = elmnt.target.tabIndex - 2;
      if (next > -1) {

        elmnt.target.form.elements[next].focus()
      }
    }
    else {
      console.log("next");

      const next = elmnt.target.tabIndex;
      if (next < 5) {
        elmnt.target.form.elements[next].focus()
      }
    }

  }

  return (
    <form onSubmit={handleSubmit}>
      <div className={styles.otpContainer}>
        <h3>
          Enter OTP
        </h3>
        <div className={styles.otpInputs}>
          <div>
            <input
              name="otp1"
              type="text"
              autoComplete="off"
              className={styles.otpInput}
              value={otpValues.otp1}
              onChange={(e) => handleChange("otp1", e)}
              tabIndex="1" maxLength="1" onKeyUp={e => inputfocus(e)}

            />
            <input
              name="otp2"
              type="text"
              autoComplete="off"
              className={styles.otpInput}
              value={otpValues.otp2}
              onChange={e => handleChange("otp2", e)}
              tabIndex="2" maxLength="1" onKeyUp={e => inputfocus(e)}

            />
            <input
              name="otp3"
              type="text"
              autoComplete="off"
              className={styles.otpInput}
              value={otpValues.otp3}
              onChange={e => handleChange("otp3", e)}
              tabIndex="3" maxLength="1" onKeyUp={e => inputfocus(e)}

            />
            <input
              name="otp4"
              type="text"
              autoComplete="off"
              className={styles.otpInput}
              value={otpValues.otp4}
              onChange={e => handleChange("otp4", e)}
              tabIndex="4" maxLength="1" onKeyUp={e => inputfocus(e)}
            />
          </div>
          <div>
            <button className={styles.otpSubmit} type="submit">
              &#8594;
            </button>
          </div>
        </div>
        <h6>
          Didn't get the code yet ?
        </h6>
        <h4>
          RESEND
        </h4>

      </div>

    </form>
  );

}
export default OtpVerification;
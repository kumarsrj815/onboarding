import React, { useEffect, useState } from 'react';
import useForm from '../hooks/useForm';

const OnboardingForm = (props) => {

  const formSubmit = () => {
    console.log("Callback function when form is submitted!");
    console.log("Form Values ", values);
    props.submittedForm();
  }

  const { handleChange, values, errors, handleSubmit } = useForm(formSubmit);

  const [isValid, setValid] = useState(false);
  const validate = () => {
    return values.name && values.email && values.aadhar && values.dob && values.pan;
  };
  useEffect(() => {
    const isValid = validate();
    setValid(isValid);
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [values]);

  return (
    <div className="onboarding-form">
      <form onSubmit={handleSubmit}>
        <div className='input-label'>Shop Owner Name</div>
        <input minLength='2' type="name" name="name" placeholder="Enter Shop Owner Name" onChange={handleChange} />
        {
          errors.name && <p>{errors.name}</p>
        }
        <div className='input-label'>Email Address</div>
        <input type="email" name="email" placeholder="test@gmail.com" onChange={handleChange} />
        {
          errors.email && <p>{errors.email}</p>
        }
        <div className='input-label'>Aadhar Number</div>
        <input type="text" minLength='10' maxLength='10' required name="aadhar" placeholder="8874676534" onChange={handleChange} />
        {
          errors.aadhar && <p>{errors.aadhar}</p>
        }
        <div className='input-label'>PAN Number</div>
        <input type="text" minLength='10' maxLength='10' required name="pan" placeholder="CHWPK6787R" onChange={handleChange} />
        {
          errors.pan && <p>{errors.pan}</p>
        }
        <div className='input-label'>Date Of Birth</div>
        <input type="date" required name="dob" onChange={handleChange} />
        {
          errors.dob && <p>{errors.dob}</p>
        }
        <input disabled={!isValid} type="submit" value="CONTINUE" className={!isValid ? 'submit' : 'active-btn'} />
      </form>
    </div>
  );
}

export default OnboardingForm;